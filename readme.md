# Task 1
Use phpmyadmin or adminer to create a database.

# Task 2
* insert to table
```sql
insert into tasks (content, completed) values ("Learn Python", 0);
insert into tasks (content, completed) values ("Learn JavaScript", 1);
```
* retrieve data from table
```sql
select * from tasks;
```
# Task 3
## prerequisites

* install python3 and pip3
* install mysql-connector-python

```bash
pip install mysql-connector-python

python app.py
```

# Task 4
## prerequisites
* install dependencies - flask, json
```bash
pip install flask json
```