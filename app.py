import mysql.connector
from mysql.connector import Error

'''
        fetch records from db, uses cursor created by connection
'''
def fetchRecords(cursor):
    query = "select * from tasks"
    cursor.execute(query)
    records = cursor.fetchall()

    print("TASKS")
    for row in records:
        print(f'id: {row[0]}\t\tcontent: {row[1]}\t\tcompleted: {row[2]}')


'''
        insert new data to db
        @input connection - instance of db connection
        @input cursor - instance of cursor in the connection
        @input content - new value to be inserted
'''
def insertData(connection, cursor, content):
    query = f"insert into tasks (content, completed) values ('{content}',0)"
    cursor.execute(query)
    connection.commit()


'''
        initialize app
'''
def init():
    try:
        connection = mysql.connector.connect(host='localhost',
                                             database='tasks',
                                             user='root',
                                             password='')

        cursor = connection.cursor()

        fetchRecords(cursor)
        insertData(connection, cursor, "Learn Flask")
        print("\nafter insertion")
        fetchRecords(cursor)

    except Error as e:
        print("Error in db connection", e)

    finally:
        if (connection.is_connected()):
            connection.close()
            cursor.close()


init()
